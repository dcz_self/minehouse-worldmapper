use crate::{
    color::Color, config::Config, mapblock::analyze_positions,
    terrain::Terrain, terrain::TerrainCell,
};
use crate::raing::Presentation;
use async_std::task;
use futures::future::join_all;
use image::RgbaImage;
use minetestworld::MAPBLOCK_LENGTH;
use minetestworld::{MapData, Position};
use split::{CHUNK_AREA_Y, Block};
use split::array::BlockSize2D;
use std::collections::BinaryHeap;
use std::error::Error;
use std::sync::Arc;

async fn generate_terrain_chunk(
    p: &'static impl Presentation,
    config: Arc<Config>,
    map: Arc<MapData>,
    x: i16,
    z: i16,
    mut ys: BinaryHeap<i16>,
) -> (i16, i16, BlockSize2D<TerrainCell>) {
    let mut chunk = [Default::default(); CHUNK_AREA_Y];

    while let Some(y) = ys.pop() {
        match map.get_mapblock(Position { x, y, z }).await {
            Ok(mapblock) => {
                chunk = p.compute_mapblock(&mapblock, &config, y * MAPBLOCK_LENGTH as i16, chunk)
            }
            // An error here is noted, but the rendering continues
            Err(e) => log::error!("Error reading mapblock at {x},{y},{z}: {e}"),
        }
        if p.done(&chunk, &config) {
            break;
        }
    }
    (x, z, BlockSize2D(p.strip(chunk)))
}

/// Renders the surface colors of the terrain along with its heightmap
pub async fn compute_terrain(map: MapData, config: &Config, p: &'static (impl Presentation + Send + Sync)) -> Result<Terrain, Box<dyn Error>> {
    let mapblock_positions = map.all_mapblock_positions().await;
    let (mut xz_positions, bbox) = analyze_positions(mapblock_positions).await?;
    log::info!("{bbox:?}");
    let mut terrain = Terrain::new(
        Block((bbox.x.start, bbox.z.start)),
        Block((bbox.x.len() + 1, bbox.z.len() + 1)),
    );

    let config = Arc::new(config.clone());
    let map = Arc::new(map);
    let mut chunks = join_all(xz_positions.drain().map(|((x, z), ys)| {
        let config = config.clone();
        let map = map.clone();
        task::spawn(generate_terrain_chunk(p, config, map, x, z, ys))
    }))
    .await;

    log::info!("Finishing surface map");
    for (x, z, chunk) in chunks.drain(..) {
        terrain.insert_chunk(Block((x, z)), chunk)
    }
    let terrain = p.stage2(Arc::into_inner(map).unwrap(), terrain, &config);
    Ok(terrain)
}

#[derive(thiserror::Error, Debug)]
pub enum RenderingError {
    #[error("width has to fit into u32")]
    WidthTooBig(std::num::TryFromIntError),
    #[error("height has to fit into u32")]
    HeightTooBig(std::num::TryFromIntError),
}

fn shade(color: &mut Color, height_diff: i16) {
    let mag: u8 = height_diff.abs().try_into().unwrap_or(0);
    if mag > 0 && mag < 3 {
        let mag: u8 = mag.try_into().unwrap();
        let op = if height_diff.signum() > 0 {
            Color::lighten_up
        } else {
            Color::darken
        };
        op(color, mag.saturating_mul(8));
    }
}

impl Terrain {
    fn heightdiff(&self, x: u32, y: u32) -> i16 {
        let x_diff = self.height_diff_x(x, y).unwrap_or(0);
        let y_diff = self.height_diff_y(x, y).unwrap_or(0);
        x_diff + y_diff
    }

    pub fn render(&self, config: &Config) -> Result<RgbaImage, RenderingError> {
        let mut image = RgbaImage::new(
            self.width_nodes()
                .try_into()
                .map_err(RenderingError::WidthTooBig)?,
            self.height_nodes()
                .try_into()
                .map_err(RenderingError::HeightTooBig)?,
        );
        for y in 0..self.height_nodes() {
            let y = y as u32;
            for x in 0..self.width_nodes() {
                let x = x as u32;
                let mut col = self.get_color(x, y).unwrap_or(config.background_color);
                if config.hill_shading.enabled {
                    shade(&mut col, self.heightdiff(x, y));
                }
                *image.get_pixel_mut(x, y) = col.with_background(&config.background_color).0;
            }
        }
        Ok(image)
    }
}
