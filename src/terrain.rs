use crate::color::Color;
use minetestworld::MAPBLOCK_LENGTH;
use split::array::BlockSize2D;
use split::{CHUNK_AREA_Y, Block, Node};
use std::cmp;

/// Describes the top-down view on the map
#[derive(Default, Clone, Copy, Debug)]
pub struct TerrainCell {
    pub ground_color: Color,
    pub height: Option<i16>,
}

impl TerrainCell { 
    pub fn alpha(&self) -> u8 {
        self.ground_color.alpha()
    }

    /// Sets the terrain elevation, if not present already
    pub fn set_height(&mut self, height: i16) {
        if self.height.is_none() {
            self.height = Some(height)
        }
    }
}

#[derive(Debug, Clone)]
pub struct Size {
    x: usize,
    z: usize,
}

#[derive(Clone)]
pub struct Terrain {
    // x, z
    offset: Block<(i16, i16)>,
    size: Block<Size>,
    flat_data: Vec<TerrainCell>,
}

impl Terrain {
    pub fn new(offset: Block<(i16, i16)>, Block((xsize, zsize)): Block<(usize, usize)>) -> Self {
        Terrain {
            offset,
            size: Block(Size {x: xsize, z: zsize}),
            flat_data: vec![Default::default(); xsize * zsize * CHUNK_AREA_Y],
        }
    }
    
    pub fn iter_block_indices(&self)
        -> impl Iterator<Item=Block<(i16, i16)>>
    {
        let Block((ox, oz)) = self.offset;
        let Block(bsize) = &self.size;
        let xs = ox..(ox + bsize.x as i16);
        let zs = oz..(oz + bsize.z as i16);
        zs.flat_map(move |z| xs.clone().map(move |x| Block((x, z))))
    }
    
    pub fn width_nodes(&self) -> usize {
        self.size.0.x * MAPBLOCK_LENGTH as usize
    }

    pub fn height_nodes(&self) -> usize {
        self.size.0.z * MAPBLOCK_LENGTH as usize
    }
    
    pub fn size_nodes(&self) -> Node<Size> {
        Node(Size {
            x: self.size.0.x * MAPBLOCK_LENGTH as usize,
            z: self.size.0.z * MAPBLOCK_LENGTH as usize,
        })
    }
 
    fn cell_idx(&self, x: i32, z: i32) -> Option<usize> {
        let Block((bx, bz)) = self.offset;
        let x = x - bx as i32 * MAPBLOCK_LENGTH as i32;
        let z = z - bz as i32 * MAPBLOCK_LENGTH as i32;
        if x < 0 || z < 0 {
            None
        } else {
            Some(
                (z as usize) * (self.size.0.x * MAPBLOCK_LENGTH as usize)
                    + x as usize
            )
        }
    }

    /// Panics if out of bounds
    fn get_cell_mut(&mut self, x: u32, z: u32) -> &mut TerrainCell {
        self.flat_data
            .get_mut(z as usize * (self.size.0.x * MAPBLOCK_LENGTH as usize) + x as usize)
            .unwrap_or_else(|| panic!("Trying to get Terrain[{x}, {z}]"))
    }

    pub fn get_color(&self, x: u32, z: u32) -> Option<Color> {
        Some(self.get_cell(x, z)?.ground_color)
    }

    pub fn get_cell(&self, x: u32, z: u32) -> Option<&TerrainCell> {
        self.flat_data.get(z as usize * (self.size.0.x * MAPBLOCK_LENGTH as usize) + x as usize)
    }
    
    pub fn get_cell_abs(&self, Node((x, z)): Node<(i32, i32)>) -> Option<&TerrainCell> {
        let idx = self.cell_idx(x, z)?;
        self.flat_data.get(idx)
    }
    
    pub fn set_cell(&mut self, Node((x, z)): Node<(i32, i32)>, cell: TerrainCell) {
        let idx = self.cell_idx(x, z).unwrap();
        self.flat_data[idx] = cell;
    }

    /// Panics if out of bounds
    pub fn insert_chunk(
        &mut self,
        Block((bx, bz)): Block<(i16, i16)>,
        chunk: BlockSize2D<TerrainCell>,
    ) {
        let nx = bx as i32 * MAPBLOCK_LENGTH as i32;
        let nz = bz as i32 * MAPBLOCK_LENGTH as i32;
        for (Node((z, x)), cell)
        in BlockSize2D::<TerrainCell>::iter_indices()
            .map(|idx| (idx, *chunk.get(idx).unwrap()))
        {
            self.set_cell(Node((x as i32 + nx, z as i32 + nz)), cell);
        }
    }

    pub fn height_diff_x(&self, x: u32, z: u32) -> Option<i16> {
        let a = self
            .get_cell(x.saturating_sub(1), z)
            .or_else(|| self.get_cell(x, z))?;
        let this_cell = self.get_cell(x, z)?.height?;
        let b = self.get_cell(x + 1, z).or_else(|| self.get_cell(x, z))?;
        let ascent = cmp::max(b.height? - this_cell, 0);
        let descent = cmp::max(a.height? - this_cell, 0);
        Some(ascent.saturating_sub(descent))
    }

    pub fn height_diff_y(&self, x: u32, z: u32) -> Option<i16> {
        let a = self
            .get_cell(x, z.saturating_sub(1))
            .or_else(|| self.get_cell(x, z))?;
        let this_cell = self.get_cell(x, z)?.height?;
        let b = self.get_cell(x, z + 1).or_else(|| self.get_cell(x, z))?;
        let ascent = cmp::max(b.height? - this_cell, 0);
        let descent = cmp::max(a.height? - this_cell, 0);
        Some(ascent.saturating_sub(descent))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use assert_matches::assert_matches;
    #[test]
    fn oob_neg() {
        let t = Terrain::new(Block((0,0)), Block((1,1)));
        assert_matches!(t.get_cell_abs(Node((-1, 0))), None);
        assert_matches!(t.get_cell_abs(Node((0, -1))), None);
    }
}