use async_std::fs;
use async_std::path::PathBuf;
use clap::{Parser, ValueEnum};
use log_err::LogErrResult;
use minetestworld::World;

mod color;
mod mapblock;
mod raing;
mod render;
use render::compute_terrain;
mod config;
use config::Config;
mod terrain;

#[derive(ValueEnum, Clone, Debug)]
enum Render {
    /// No frills basic rendering.
    Basic,
    /// No color, only height
    Height,
    /// Sees through water
    Underwater,
    /// Look through the first solid layer. Recommended postproc shade
    Noceiling,
    /// Shows cave entrances. Needs Entrances postproc
    Entrances,
}

#[derive(ValueEnum, Clone, Debug)]
enum Postproc {
    None,
    Stairs,
    Shade,
    /// Debug only
    Height,
    /// Shows cave entrances. Needs Entrances render
    Entrances,
}

/// Render a minetest world into a map
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// World directory
    #[clap(short, long)]
    world: PathBuf,

    /// Config file
    #[clap(short, long)]
    config: PathBuf,

    /// The file in which to write the result
    #[clap(short, long)]
    output: PathBuf,
    
    /// Render type
    #[clap(short, long)]
    render: Render,
    
    /// Postprocessing
    #[clap(short, long)]
    post: Postproc,
}

#[async_std::main]
async fn main() {
    pretty_env_logger::init();
    let args = Args::parse();
    let config = fs::read_to_string(&args.config)
        .await
        .log_expect("reading config");
    let config: Config = toml::from_str(&config).log_expect("parsing config");
    let world = World::open(args.world);
    let map = world.get_map_data().await.log_expect("opening world data");
    let terrain_map = match args.render {
        Render::Basic => {
            compute_terrain(map, &config, &raing::Basic)
                .await
                .log_expect("generating terrain map")
        },
        Render::Underwater => {
            compute_terrain(map, &config, &raing::Underwater)
                .await
                .log_expect("generating terrain map")
        },
        Render::Noceiling => {
            compute_terrain(map, &config, &raing::Noceiling)
                .await
                .log_expect("generating terrain map")
        },
        Render::Entrances => {
            compute_terrain(map, &config, &raing::Entrances)
                .await
                .log_expect("generating terrain map")
        },
        Render::Height => {
            compute_terrain(map, &config, &raing::Height)
                .await
                .log_expect("generating terrain map")
        },
    };
    let postfn: fn(_, _) -> Result<image::RgbaImage,_> = match args.post {
        Postproc::None => raing::copy as fn(_, _)->_,
        Postproc::Height => raing::height as fn(_, _)->_,
        Postproc::Stairs => raing::basic as fn(_, _)->_,
        Postproc::Shade => raing::shadows as fn(_, _)->_,
        Postproc::Entrances => raing::entrances as fn(_, _)->_,
    };
    let picture = postfn(&terrain_map, &config).log_expect("rendering map");
    log::info!("Saving image");
    picture.save(&args.output).log_expect("saving image");
}
