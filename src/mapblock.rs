use crate::color::Color;
use crate::config::Config;
use crate::terrain::TerrainCell;
use futures::stream::{Stream, StreamExt};
use minetestworld::MAPBLOCK_LENGTH;
use minetestworld::{MapBlock, Position};
use split::CHUNK_AREA_Y;
use std::collections::{BinaryHeap, HashMap};
use std::ops::Range;

#[derive(Debug)]
pub struct Bbox {
    pub x: Range<i16>,
    pub z: Range<i16>,
}

impl Bbox {
    fn from_opt_ranges(x: Option<Range<i16>>, z: Option<Range<i16>>) -> Option<Bbox> {
        Some(Bbox { x: x?, z: z? })
    }
}

fn update_range(range: &mut Option<Range<i16>>, new_value: i16) {
    if let Some(range) = range {
        if new_value < range.start {
            range.start = new_value;
        } else if new_value >= range.end {
            range.end = new_value + 1;
        }
    } else {
        *range = Some(new_value..new_value + 1);
    }
}

/// Allows efficient traversing the map from above
///
/// For a given (x,z) key, the value will be a max heap of all y values.
type SortedPositions = HashMap<(i16, i16), BinaryHeap<i16>>;

/// Analyzes the given position stream and returns its Bbox and SortedPosition
///
/// Takes a stream that yields Result<Position, _>.
pub(crate) async fn analyze_positions<S, E>(mut positions: S) -> Result<(SortedPositions, Bbox), E>
where
    S: Stream<Item = Result<Position, E>> + Unpin,
{
    let mut sorted_positions = HashMap::new();
    let (mut x_range, mut z_range) = (None, None);
    while let Some(pos) = positions.next().await {
        let pos = pos?;
        update_range(&mut x_range, pos.x);
        update_range(&mut z_range, pos.z);
        let key = (pos.x, pos.z);
        let y_stack = sorted_positions.entry(key).or_insert_with(BinaryHeap::new);
        y_stack.push(pos.y);
    }
    Ok((
        sorted_positions,
        Bbox::from_opt_ranges(x_range, z_range).unwrap_or(Bbox { x: 0..0, z: 0..0 }),
    ))
}

// HashMap needs to go if the algorithm moves to the GPU because the backing storage is not stable, and the map needs to be passed to GPU.

// Not much faster on 2 out-of-order cores than HashMap
struct VecMap<T>(Vec<(u16, T)>);

impl<T> VecMap<T> {
    fn get(&self, item: &u16) -> Option<&T> {
        self.0.iter().find(|(i, _)| *i == *item).map(|(_,v)| v)
    }
}

impl<T> FromIterator<(u16, T)> for VecMap<T> {
    fn from_iter<I: IntoIterator<Item = (u16, T)>>(iter: I) -> Self {
        Self(Vec::from_iter(iter))
    }
}

// Also not better than HashMap on 2 OoO cores
#[derive(Debug)]
pub struct SortedVecMap<T>(Vec<(u16, T)>);

impl<T> SortedVecMap<T> {
    pub fn get(&self, item: &u16) -> Option<&T> {
        self.0.binary_search_by_key(&item, |(k, _v)| k)
            .map(|i| &self.0[i].1)
            .ok()
    }
    pub fn len(&self) -> usize {
        self.0.len()
    }
}

impl<T> FromIterator<(u16, T)> for SortedVecMap<T> {
    fn from_iter<I: IntoIterator<Item = (u16, T)>>(iter: I) -> Self {
        let mut v = Vec::from_iter(iter);
        v.sort_unstable_by_key(|(k, _v)| *k);
        Self(v)
    }
}

pub fn compute_mapblock(
    mapblock: &MapBlock,
    config: &Config,
    base_height: i16,
    acc: [TerrainCell; CHUNK_AREA_Y],
) -> [TerrainCell; CHUNK_AREA_Y] {
    // Pre-compute colors for all block names except those not in config
    let colors: SortedVecMap<_>
        = mapblock.name_id_mappings.iter()
            .filter_map(|(k, v)|
                config.get_color(v).map(|c| (*k, *c))
            )
            .collect();
    /*// makes very little difference
     * if colors.len() == 0 {
        for mut c in acc {
            c.set_height(base_height + MAPBLOCK_LENGTH as i16);
        }
        return acc;
    }*/
    if config.legacy {
        cpublock(mapblock, config, |u| colors.get(&u), base_height, acc)
    } else {
        fublock(mapblock, config, colors, base_height, acc)
    }
}

#[cfg(not(feature = "futhark"))]
fn fublock(
    mapblock: &MapBlock,
    config: &Config,
    colors: SortedVecMap<Color>,
    base_height: i16,
    acc: [TerrainCell; CHUNK_AREA_Y],
) -> [TerrainCell; CHUNK_AREA_Y] {
    panic!("Feature futhark not enabled");
}

#[cfg(feature = "futhark")]
fn fublock(
    mapblock: &MapBlock,
    config: &Config,
    colors: SortedVecMap<Color>,
    base_height: i16,
    acc: [TerrainCell; CHUNK_AREA_Y],
) -> [TerrainCell; CHUNK_AREA_Y] {
    let conf = rain::raw::Config::<rain::raw::backends::MultiCore>::new();
    let ctx = rain::raw::Context::new(conf).unwrap();
    let terrain = mapblock.param0;
    let out = ctx.entry_rain(
        &rain::raw::Array_U16_1D::new(
            &ctx,
            &colors.0.iter().map(|(i,_)| *i).collect::<Vec<_>>(),
            colors.len(),
        ),
        &rain::raw::Array_U32_1D::new(
            &ctx,
            &colors.0.iter()
                .map(|(_, c)| c.to_u32())
                .collect::<Vec<_>>(),
            colors.len(),
        ),
        &rain::raw::Array_U32_2D::new(
            &ctx,
            &acc.iter()
                .map(|c| c.ground_color.to_u32())
                .collect::<Vec<_>>(),
            16, 16,
        ),
        &rain::raw::Array_I32_2D::new(
            &ctx,
            &acc.iter()
                .map(|c| c.height.unwrap_or(base_height) as i32)
                .collect::<Vec<_>>(),
            16, 16,
        ),
        &rain::raw::Array_U16_1D::new(&ctx, &terrain, 4096)
    ).unwrap();
    ctx.sync();
    let mut heights = [0;256];
    out.0.values(&mut heights).unwrap();
    let mut colors = [0u32;256];
    out.1.values(&mut colors).unwrap();
    let mut out = [TerrainCell::default(); 256]; heights.into_iter()
        .zip(colors.into_iter())
        .map(|(h,c)| TerrainCell {
            height: Some(h as i16),
            ground_color: Color::from_u32(c),
        })
        .zip(out.iter_mut())
        .for_each(|(c, o)| *o = c);
    out
}    

pub fn cpublock<'a>(
    mapblock: &MapBlock,
    config: &Config,
    get_color: impl Fn(u16)->Option<&'a Color> + 'a,
    base_height: i16,
    mut acc: [TerrainCell; CHUNK_AREA_Y],
) -> [TerrainCell; CHUNK_AREA_Y] {
    for z in 0..MAPBLOCK_LENGTH {
        for x in 0..MAPBLOCK_LENGTH {
            let col_index = (x + MAPBLOCK_LENGTH * z) as usize;
            if acc[col_index].alpha() == 255 {
                continue;
            }

            for y in (0..MAPBLOCK_LENGTH).rev() {
                let node = mapblock.get_node_at(Position::new(x, y, z));
                if let Some(color) = get_color(node.param0.raw_id()) {
                    acc[col_index].ground_color = acc[col_index].ground_color.with_background(color);
                    if config.hill_shading.enabled
                        && acc[col_index].alpha() > config.hill_shading.min_alpha
                    {
                        acc[col_index].set_height(base_height + y as i16);
                    }
                    if acc[col_index].alpha() > config.sufficient_alpha {
                        acc[col_index].set_height(base_height + y as i16);
                        break;
                    }
                }
            }
        }
    }
    acc
}
