use minetestworld::{World, Position};
use minetestworld::reexport::stream::{Stream, StreamExt};
use async_std::task;

type LocalToGlobal<T> = Vec<(u16, T)>;

type LocalToGlobalId = LocalToGlobal<u16>;

type LocalLUT<T> = [T; 1<<16];

fn main() {
    let blockpos = Position {
        x: 0,
        y: 0,
        z: 0,
    };
    let blockpos2 = Position {
        x: 25,
        y: 0,
        z: 0,
    };


    task::block_on(async {
        let world = World::open("/mnt/space/rhn/minetest_world");
        let mapdata = world.get_map_data().await.unwrap();
        let pos = mapdata
            .all_mapblock_positions().await
            .collect::<Vec<_>>().await;
        for p in pos {
            let p = p.unwrap();
            match mapdata.get_mapblock(p).await {
                Ok(b) => {
                    let c = b.content_names().count();
                    println!("{}", c);
                },
                Err(e) => {eprintln!("{}", e)},
            }
        }
    });
}