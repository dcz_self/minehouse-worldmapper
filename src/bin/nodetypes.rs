use minetestworld::World;
use minetestworld::reexport::stream::StreamExt;
use async_std::task;
use std::collections::HashSet;

type LocalToGlobal<T> = Vec<(u16, T)>;

type LocalToGlobalId = LocalToGlobal<u16>;

type LocalLUT<T> = [T; 1<<16];

fn main() {
    task::block_on(async {
        let world = World::open("/mnt/space/rhn/minetest_world");
        let mapdata = world.get_map_data().await.unwrap();
        let pos = mapdata
            .all_mapblock_positions().await
            .collect::<Vec<_>>().await;
        let mut names = HashSet::new();
        for p in pos {
            let p = p.unwrap();
            match mapdata.get_mapblock(p).await {
                Ok(b) => {
                    names.extend(
                        b.content_names().map(|n| String::from_utf8(n.into()).unwrap_or_default())
                    );
                },
                Err(e) => {eprintln!("{}", e)},
            }
        }
        dbg!(names);
    });
}