use minetestworld::{World, Position};
use async_std::task;
use std::str;

type LocalToGlobal<T> = Vec<(u16, T)>;

type LocalToGlobalId = LocalToGlobal<u16>;

type LocalLUT<T> = [T; 1<<16];

fn main() {
    let blockpos = Position {
        x: 0,
        y: 0,
        z: 0,
    };
    let blockpos2 = Position {
        x: 25,
        y: 0,
        z: 0,
    };

    task::block_on(async {
        let world = World::open("/mnt/space/rhn/minetest_world");
        let mapdata = world.get_map_data().await.unwrap();
        let block = mapdata.get_mapblock(blockpos).await.unwrap();
        dbg!(
            &block.name_id_mappings.into_iter()
                .map(|(k, v)| (
                    k,
                    String::from_utf8(v).unwrap_or_default(),
                ))
                .collect::<Vec<_>>()
        );
        let block = mapdata.get_mapblock(blockpos2).await.unwrap();
        dbg!(
            &block.name_id_mappings.iter()
                .map(|(k, v)| (
                    k,
                    str::from_utf8(v).unwrap_or_default(),
                ))
                .collect::<Vec<_>>()
        );
        for (pos, node) in block.iter_nodes() {
            //println!("{pos:?}, {node:?}, {}", str::from_utf8(&node.param0).unwrap_or_default());
        }
    });
}