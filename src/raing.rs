/*! Rainy algorithms, traversing blocks from the sky to the bedrock. */
use async_std::task;
use crate::color::Color;
use crate::config::Config;
use crate::mapblock;
use crate::mapblock::SortedVecMap;
use crate::terrain::TerrainCell;
use minetestworld::{MapData, MapBlock, Position};
use minetestworld::MAPBLOCK_LENGTH;
use split::{CHUNK_AREA_Y, Block, Node};
use split::array::BlockSize2D;
use std::collections::HashMap;


pub trait Presentation {
    type Acc: Default + Copy + Send + Sync;
    
    fn compute_mapblock(
        &self,
        mapblock: &MapBlock,
        config: &Config,
        base_height: i16,
        acc: [Self::Acc; CHUNK_AREA_Y],
    ) -> [Self::Acc; CHUNK_AREA_Y];
    
    fn done(&self, acc: &[Self::Acc; CHUNK_AREA_Y], config: &Config) -> bool;
    
    fn strip(&self, acc: [Self::Acc; CHUNK_AREA_Y]) -> [TerrainCell; CHUNK_AREA_Y];
    
    fn stage2(&self, map: MapData, acc: Terrain, _config: &Config) -> Terrain  {
        acc
    }
}

pub struct Basic;
impl Presentation for Basic {
    type Acc = TerrainCell;
    fn compute_mapblock(
        &self,
        mapblock: &MapBlock,
        config: &Config,
        base_height: i16,
        acc: [Self::Acc; CHUNK_AREA_Y],
    ) -> [Self::Acc; CHUNK_AREA_Y] {
        mapblock::compute_mapblock(mapblock, config, base_height, acc)
    }
    
    fn done(&self, acc: &[Self::Acc; CHUNK_AREA_Y], config: &Config) -> bool {
        acc.iter().all(|c| c.alpha() > config.sufficient_alpha)
    }
    
    fn strip(&self, acc: [Self::Acc; CHUNK_AREA_Y]) -> [TerrainCell; CHUNK_AREA_Y] {
        acc
    }
}

pub struct Height;

impl Presentation for Height {
    type Acc = TerrainCell;
    fn compute_mapblock(
        &self,
        mapblock: &MapBlock,
        config: &Config,
        base_height: i16,
        acc: [Self::Acc; CHUNK_AREA_Y],
    ) -> [Self::Acc; CHUNK_AREA_Y] {
        let colors: SortedVecMap<_>
        = mapblock.name_id_mappings.iter()
            .filter_map(|(k, v)|
                config.get_color(v).map(|c| (*k, *c))
            )
            .collect();
            
            use std::collections::HashSet;
        let air_ids: HashSet<_>
            = mapblock.name_id_mappings.iter()
                .filter_map(|(id, name)|
                    match config.cave_air.iter().find(|n| n.as_bytes() == *name) {
                        Some(_) => Some(*id),
                        _ => None,
                    }
                )
                .collect();

        mapblock::cpublock(
            mapblock,
            config,
            |id| if air_ids.contains(&id) {
                None
            } else {
                // FIXME: it doesn't work if this lookup is replaced by a constant color. Probably makes the whole thing slower than necessary.
                colors.get(&id)
            },
            base_height,
            acc,
        )
    }
    fn done(&self, acc: &[Self::Acc; CHUNK_AREA_Y], _config: &Config) -> bool {
        acc.iter().all(|c| c.alpha() >= 255)
    }
    
    fn strip(&self, acc: [Self::Acc; CHUNK_AREA_Y]) -> [TerrainCell; CHUNK_AREA_Y] {
        acc
    }
}

pub struct Underwater;

impl Presentation for Underwater {
    type Acc = (TerrainCell, u8);
    fn compute_mapblock(
        &self,
        mapblock: &MapBlock,
        config: &Config,
        base_height: i16,
        mut acc: [Self::Acc; CHUNK_AREA_Y],
    ) -> [Self::Acc; CHUNK_AREA_Y] {
        let colors: SortedVecMap<_>
        = mapblock.name_id_mappings.iter()
            .filter_map(|(k, v)|
                config.get_color(v).map(|c| (*k, *c))
            )
            .collect();
        let waters: SortedVecMap<_>
        = mapblock.name_id_mappings.iter()
            .filter_map(|(k, v)|
                config.get_water(v).map(|c| (*k, *c))
            )
            .collect();
        for z in 0..MAPBLOCK_LENGTH {
            for x in 0..MAPBLOCK_LENGTH {
                let col_index = (x + MAPBLOCK_LENGTH * z) as usize;
                if acc[col_index].0.alpha() == 255 {
                    continue;
                }

                let mut water_depth = acc[col_index].1;
                let mut color = acc[col_index].0.ground_color;
                for y in (0..MAPBLOCK_LENGTH).rev() {
                    let node = mapblock.get_node_at(Position::new(x, y, z));
                    if let Some(node_color) = colors.get(&node.param0.raw_id()) {
                        acc[col_index].0.height = Some(base_height + y as i16);
                        color = if let Some(depth_color) = waters.get(&node.param0.raw_id()) {
                            water_depth += 1;
                            if water_depth % 4 == 1 {
                                let c = if water_depth / 4 == 0 {
                                    node_color
                                } else {
                                    depth_color
                                };
                                
                                color.with_background(c)
                            } else {
                                color
                            }
                        } else {
                            water_depth = 0;
                            color.with_background(node_color)
                        };
                        if color.alpha() > config.sufficient_alpha {
                            break;
                        }
                    }
                }
                acc[col_index].1 = water_depth;
                acc[col_index].0.ground_color = color;
            }
        }
        acc
    }
    fn done(&self, acc: &[Self::Acc; CHUNK_AREA_Y], config: &Config) -> bool {
        acc.iter().all(|(c, _w)| c.alpha() > config.sufficient_alpha)
    }
    fn strip(&self, acc: [Self::Acc; CHUNK_AREA_Y]) -> [TerrainCell; CHUNK_AREA_Y] {
        let mut o = [TerrainCell::default(); CHUNK_AREA_Y];
        acc.into_iter()
            .map(|(c, _)| c)
            .zip(o.iter_mut())
            .for_each(|(c, o)| *o = c);
        o
    }
}


pub struct Noceiling;

#[derive(Debug, Clone, Copy)]
pub enum CeilingStage {
    Approaching,
    Ceiling,
    Through,
}

impl Default for CeilingStage {
    fn default() -> Self {
        Self::Approaching
    }
}

impl Presentation for Noceiling {
    type Acc = (TerrainCell, CeilingStage);
    fn compute_mapblock(
        &self,
        mapblock: &MapBlock,
        config: &Config,
        base_height: i16,
        mut acc: [Self::Acc; CHUNK_AREA_Y],
    ) -> [Self::Acc; CHUNK_AREA_Y] {
        let colors: SortedVecMap<_>
        = mapblock.name_id_mappings.iter()
            .filter_map(|(k, v)|
                config.get_color(v).map(|c| (*k, *c))
            )
            .collect();
        for z in 0..MAPBLOCK_LENGTH {
            for x in 0..MAPBLOCK_LENGTH {
                let col_index = (x + MAPBLOCK_LENGTH * z) as usize;
                if acc[col_index].0.alpha() == 255 {
                    continue;
                }

                let mut ceiling_stage = acc[col_index].1;
                let mut color = acc[col_index].0.ground_color;
                for y in (0..MAPBLOCK_LENGTH).rev() {
                    let node = mapblock.get_node_at(Position::new(x, y, z));
                    let node_color = colors.get(&node.param0.raw_id());
                    ceiling_stage = match (ceiling_stage, node_color) {
                        (CeilingStage::Approaching, Some(_))
                            => CeilingStage::Ceiling,
                        (CeilingStage::Ceiling, None)
                            => CeilingStage::Through,
                        (CeilingStage::Through, Some(node_color)) => {
                            acc[col_index].0.height = Some(base_height + y as i16);
                            color = color.with_background(node_color);
                            if color.alpha() > config.sufficient_alpha {
                                break;
                            }
                            CeilingStage::Through
                        },
                        (stage, _) => stage,
                    }
                }
                acc[col_index].1 = ceiling_stage;
                acc[col_index].0.ground_color = color;
            }
        }
        acc
    }
    fn done(&self, acc: &[Self::Acc; CHUNK_AREA_Y], config: &Config) -> bool {
        acc.iter().all(|(c, _w)| c.alpha() > config.sufficient_alpha)
    }
    fn strip(&self, acc: [Self::Acc; CHUNK_AREA_Y]) -> [TerrainCell; CHUNK_AREA_Y] {
        let mut o = [TerrainCell::default(); CHUNK_AREA_Y];
        acc.into_iter()
            .map(|(c, _)| c)
            .zip(o.iter_mut())
            .for_each(|(c, o)| *o = c);
        o
    }
}


pub struct Entrances;

mod Direction {
    pub const ZP: i16 = 0b0001;
    pub const ZN: i16 = 0b0010;
    pub const XP: i16 = 0b0100;
    pub const XN: i16 = 0b1000;
}

struct PositionedBlock {
    position: Block<(i16, i16, i16)>,
    block: MapBlock,
}

impl PositionedBlock {
    // panics when out of bounds
    fn get_node_abs(&self, pos: Node<(i32, i32, i32)>) -> minetestworld::Node<minetestworld::map_block::NodeId<'_>> {
        let Block((bx, by, bz)) = &self.position;
        let Node((nx, ny, nz)) = pos;
        let x = nx - (*bx as i32 * MAPBLOCK_LENGTH as i32);
        let y = ny - (*by as i32 * MAPBLOCK_LENGTH as i32);
        let z = nz - (*bz as i32 * MAPBLOCK_LENGTH as i32);
        let to_blocksize = |d: i32| if d < 0 {
            panic!("Index too small {} {} {} {}, {:?}, {:?}", d, x, y, z, pos, self.position.with_inner(Node((0, 0, 0))));
        } else if d >= MAPBLOCK_LENGTH as i32 {
            panic!("Index too large");
        } else {
            d as i16
        };
        self.block.get_node_at(Position {
            x: to_blocksize(x),
            y: to_blocksize(y),
            z: to_blocksize(z),
        })
    }
}

impl Presentation for Entrances {
    type Acc = TerrainCell;
    fn compute_mapblock(
        &self,
        mapblock: &MapBlock,
        config: &Config,
        base_height: i16,
        acc: [Self::Acc; CHUNK_AREA_Y],
    ) -> [Self::Acc; CHUNK_AREA_Y] {
        Height.compute_mapblock(mapblock, config, base_height, acc)
    }
    fn done(&self, acc: &[Self::Acc; CHUNK_AREA_Y], config: &Config) -> bool {
        Height.done(acc, config)
    }
    fn strip(&self, acc: [Self::Acc; CHUNK_AREA_Y]) -> [TerrainCell; CHUNK_AREA_Y] {
        Height.strip(acc)
    }

    fn stage2(&self, map: MapData, terrain: Terrain, config: &Config) -> Terrain {
        let mut out = terrain.clone();
        task::block_on(async {
            for bidx in terrain.iter_block_indices() {
                let mut blocks = HashMap::<Block<(i16, i16, i16)>, _>::new();
                for nbidx in BlockSize2D::<()>::iter_indices() {
                    let nidx = bidx.with_inner(nbidx);
                    let t = terrain.get_cell_abs(nidx).unwrap();
                    let is_entrance = match t.height {
                        None => false,
                        Some(t_height) => {
                            let t_height = t_height as i32;
                            let Node((ax, az)) = nidx;
                            let d = [(ax, az-1), (ax, az+1),(ax-1, az),(ax+1, az)];
                            let lowest_neighbor = d.into_iter()
                                .map(|pos|
                                    terrain.get_cell_abs(Node(pos))
                                        .and_then(|c| c.height)
                                        .map(|h| h as i32)
                                        .unwrap_or(t_height)
                                )
                                .min().unwrap();
                            if lowest_neighbor < t_height - 2  {
                                let bx = (ax / MAPBLOCK_LENGTH as i32) as i16;
                                let bz = (az / MAPBLOCK_LENGTH as i32) as i16;
                                // TODO: turn Y into block index
                                for by in (lowest_neighbor / MAPBLOCK_LENGTH as i32)..=((t_height - 2) / MAPBLOCK_LENGTH as i32) {
                                    let by = by as i16;
                                    let bidx = Block((bx, by, bz));
                                    if !blocks.contains_key(&bidx) {
                                        if let Ok(block) = map
                                            .get_mapblock(Position {x: bx, y: by, z: bz})
                                            .await
                                        {
                                            blocks.insert(bidx, block);
                                        }
                                    }
                                }

                                (lowest_neighbor..=t_height - 2)
                                    .filter_map(|y| {
                                        let nidx = Node((ax, y, az));
                                        let bidx = nidx.to_block();
                                        blocks.get(&bidx)
                                            .map(|block| {
                                                block.content_from_id(
                                                    PositionedBlock { block: block.clone(), position: bidx }
                                                        .get_node_abs(nidx)
                                                        .param0
                                                        .raw_id()
                                                    )
                                            })
                                    })
                                    .find(|content| {
                                        config.cave_air.iter()
                                            .find(|pattern| content == &pattern.as_bytes())
                                            .is_some()
                                    })
                                    .is_some()
                            } else {
                                false
                            }
                        }
                    };
                    out.set_cell(
                        nidx,
                        TerrainCell {
                            ground_color: Default::default(), 
                            height: Some(is_entrance as i16)
                        }
                    );
                }
            }
        });
        out
    }
}

// postprocessors
use crate::render::RenderingError;
use crate::terrain::Terrain;
use image::{Rgba, RgbaImage};
use std::cmp;

pub fn new_image(t: &Terrain) -> Result<RgbaImage, RenderingError> {
    Ok(RgbaImage::new(
        t.width_nodes()
            .try_into()
            .map_err(RenderingError::WidthTooBig)?,
        t.height_nodes()
            .try_into()
            .map_err(RenderingError::HeightTooBig)?,
    ))
}

pub fn map(t: &Terrain, f: impl Fn(&TerrainCell) -> Rgba<u8>) -> Result<RgbaImage, RenderingError> {
    let mut image = new_image(t)?;
    for z in 0..t.height_nodes() {
        let z = z as u32;
        // flipped
        let y = image.height() - z - 1;
        for x in 0..t.width_nodes() {
            let x = x as u32;
            *image.get_pixel_mut(x, y) = f(t.get_cell(x, z).unwrap())
        }
    }
    Ok(image)
}

pub fn copy(t: &Terrain, c: &crate::config::Config) -> Result<RgbaImage, RenderingError> {
    map(t, |n| 
        n.ground_color
            .with_background(&c.background_color).0
    )
}

pub fn basic(t: &Terrain, c: &crate::config::Config) -> Result<RgbaImage, RenderingError> {
    t.render(c)
}


/// Returns amount of darkening
fn shadow(t: &Terrain, x: u32, y: u32) -> i16 {
    let xmin = x.saturating_sub(4);
    let xmax = cmp::min(x + 4, t.width_nodes() as u32);
    let ymin = y.saturating_sub(4);
    let ymax = cmp::min(y + 4, t.height_nodes() as u32);
    
    let center_height = t.get_cell(x, y)
        .and_then(|c| c.height)
        .unwrap_or(0);
    
    let mut sum = 0;
    for x in xmin..xmax {
        for y in ymin..ymax {
            let diff = t.get_cell(x, y)
                .and_then(|c| c.height)
                .unwrap_or(0) - center_height;
            if diff > 0 {
                sum += diff;
            }
        }
    }
    sum / ((xmax-xmin) * (ymax-ymin)) as i16
}

/// Objects cast shadows in all directions
pub fn shadows(t: &Terrain, config: &crate::config::Config) -> Result<RgbaImage, RenderingError> {
    let mut image = new_image(t)?;
    for z in 0..t.height_nodes() {
        let z = z as u32;
        // flipped
        let y = image.height() - z - 1;
        for x in 0..t.width_nodes() {
            let x = x as u32;
            let col = t.get_color(x, z)
                .unwrap_or(config.background_color);
            let darken = (shadow(&t, x, z) as u8).saturating_mul(8);
            let col = col.to_darken(darken);
            *image.get_pixel_mut(x, y) = col.with_background(&config.background_color).0;
        }
    }
    Ok(image)
}

pub fn entrances(t: &Terrain, _config: &crate::config::Config) -> Result<RgbaImage, RenderingError> {
    map(t, |c| Rgba(match c.height {
        Some(1) => [0, 0xff, 0xff, 0xff],
        _ => [0,0,0,0],
    }))
}


pub fn height(t: &Terrain, _config: &crate::config::Config) -> Result<RgbaImage, RenderingError> {
    map(t, |c| Rgba(match c.height {
        Some(h) => [((h as u16) >> 8) as u8, h as u16 as u8, 0xff, 0xff],
        _ => [0,0,0,0],
    }))
}