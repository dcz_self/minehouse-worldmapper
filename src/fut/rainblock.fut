type color = (u8, u8, u8, u8)
type blockcolors = [16*16]color


type optional_color = #none | #color color

type~ idmap = [](u16, color)

def get_color(idmapping: idmap, id: u16) : optional_color =
    let index =
        loop acc = 0 while
            acc < length idmapping
            && id != idmapping[acc].0
        do
            acc + 1
    in
        if index < length idmapping
        then #color idmapping[index].1
        else #none

def rain (
    idmapping: idmap,
    color: blockcolors,
    height: [16*16]i32,
    blocks: [16*16*16]u16
) : blockcolors =
  let uheight : [16][16]i32 = unflatten height
  let ublocks : [16][16][16]u16 = unflatten_3d blocks
  in
    map
        (\aidx -> (
            let dcolor =
                loop (depth, color) = (0, #none) while
                    depth < 16
                    && color == #none
                do (
                    depth + 1,
                    get_color (idmapping, blocks[aidx * 16 + depth])
                )
            in match dcolor.1
                case #none -> (0, 0, 0, 0)
                case #color x -> x
        ))
        (indices height)