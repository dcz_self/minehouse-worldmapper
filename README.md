# minehouse-worldmapper

A multi-threaded mapper for Minetest with fancy features like shadows.

It generates a view of the world from above, rendering one pixel per voxel.

This project is currently tested on Linux only. If you have Windows and it doesn't work, please file a [bug report](https://gitlab.com/dcz_self/minehouse-worldmapper/-/issues). But if you have another OS and it does not work, please report also a bug.

## Features

See what's underwater, and that includes caves! Notice the stylish height lines.

![Underwater](showcase/underwater.png)

Fancy shading for cliffs (and caves)! (TODO: show height lines at the same time.)

![Cliffs](showcase/cliffs.png)

See through the top layer of ground to glimpse the mess of passages underneath (not actually so useful, but I implemented it, so it is there).

![Caves](showcase/underground.png)

And an overlay showing cave entrances... poorly. No image, too embarassing.

## Usage

First, get [cargo](https://doc.rust-lang.org/stable/cargo/) and pre-build the project:

```bash
cargo build --release
```
Then, find the path to your Minetest world. The default configuration in `config.toml` supports colors for MineClone2.

Now run the program with a path to the map directory, like:

```
cargo run --release --bin minetest-worldmapper -- --world path-to-my/minetest_world/ --config config.toml --output basic_shadows.png --render basic --post shadows
```

### Command-line arguments

Run with `--help`, like this:

```
cargo run --release --bin minetest-worldmapper -- --help
```

### Logging
Via the [`RUST_LOG`](https://docs.rs/env_logger/latest/env_logger/#enabling-logging) environment variable, you can choose one out of the log levels `trace`, `debug`, `info`, `warn`, and `error`. The default is `error`.


### Config file
If a voxel is rendered and its color are entirely determined by the config file, which is based on TOML.
An example config file is part of this repo. Its main purpose is to map the voxel content to colors.

| Config option           | Type         | Description                         |
| ----------------------- | ------------ | ----------------------------------- |
| `sufficient_alpha`      | [Integer][3] | (optional, defaults to `230`) When determining a pixel's color, stop going through transparent nodes when reaching this opacity value. Between 0 and 255. |
| `background_color`      | [String][4]  | Hex color string; either in the format "rrggbb" (full opacity) or "rrggbbaa" (color with alpha value). Serves as a fallback color if all voxels for a given pixel are exhausted and there is transparency left. |
| `hillshading.enabled`   | [Boolean][3] | (optional, defaults to `true`) Enables terrain relief visualisation. |
| `hillshading.min_alpha` | [Integer][6] | (optional, defaults to `128`) At which alpha value a node counts as "terrain" |
| `node_colors`           | [Table][5]   | Maps node [itemstrings][7] to color strings (which have the same format as `background_color`). Every node not listed here is treated like air. |

TODO: write about new options for all the fancy renderers.


## Current limitations
* LevelDB is not supported as backend.
* Only map chunks with map format version 29 (the current) are supported.

## FAQ

### **The output image is empty**

1. Are you using MineClone2? If not, add the colors for each block to node_colors in the config.
2. Are you using a special mode? Some modes are meant for overlays only and will leave a lot of space undrawn. To see what they actually do, copy them on top of the image in a bitmap manipulation program (e.g. Kolourpaint).

[1]: #config-file
[2]: https://github.com/UgnilJoZ/minetest-worldmapper/blob/main/config.example.toml
[3]: https://toml.io/en/v1.0.0#integer
[4]: https://toml.io/en/v1.0.0#string
[5]: https://toml.io/en/v1.0.0#table
[6]: https://toml.io/en/v1.0.0#boolean
[7]: https://wiki.minetest.net/Itemstrings
