def map2d [m][n] 't 'u (f : t -> u) (arr : [n][m]t) : [n][m]u =
    flatten arr |> map f |> unflatten

def indices2d [m][n] 't (arr: [m][n]t) : [m][n](i64, i64) =
    zip arr (indices arr)
        |> map (
            \(ar, idx)
            -> ar
                |> indices
                |> map (\x -> (x, idx))
        )

def unzip2d [m][n] 't 'u (arr: [m][n](t,u)) : ([m][n]t, [m][n]u) = 
    let (a, b) = arr |> flatten |> unzip
    in (unflatten a, unflatten b)


type color = u32
type blockcolors = [16][16]color

def r(c: color) : u8 = u8.u32(c >> 24)
def g(c: color) : u8 = u8.u32(c >> 16)
def b(c: color) : u8 = u8.u32(c >> 8)
def a(c: color) : u8 = u8.u32(c)

def color_new(r: u8, g: u8, b: u8, a: u8) : u32 =
    u32.u8(r) << 24 | u32.u8(g) << 16 | u32.u8(b) << 8 | u32.u8(a)

type optional_color = #none | #color color

-- TODO: to_srgb,  from_srgb, with_bg_srgb

def with_bg(top: color, bottom: color) : color =
    match a(top)
    -- special cases may bring speedup even on SIMD when there are not many nodes with transparency at the same depth
        case 255 -> top
        case 0 -> bottom
        case _ ->
        -- from https://en.wikipedia.org/wiki/Alpha_compositing
            let fore_alpha = f32.u8(a(top)) / 255.0
            let back_alpha = 1.0 - fore_alpha
            let a = fore_alpha + back_alpha * f32.u8(a(bottom)) / 255.0
            let r = (fore_alpha * f32.u8(r(top)) + back_alpha * f32.u8(r(bottom))) / a
            let g = (fore_alpha * f32.u8(g(top)) + back_alpha * f32.u8(g(bottom))) / a
            let b = (fore_alpha * f32.u8(b(top)) + back_alpha * f32.u8(b(bottom))) / a
            in color_new(u8.f32(r), u8.f32(g), u8.f32(b), u8.f32(a * 255.0))

type~ idmap = [](u16, color)

def get_color(idmapping: idmap, id: u16) : optional_color =
    let index =
        loop acc = 0 while
            acc < length idmapping
            && id != idmapping[acc].0
        do
            acc + 1
    in
        if index < length idmapping
        then #color idmapping[index].1
        else #none

def isnone(c: optional_color) : bool = match c
    case #none -> true
    case #color _ -> false
       
def is_opaque(c: color) : bool = a(c) >= 245 -- TODO: check 255 speed
       
-- returns depth relative to block top, color
def raincol (
    idmapping: idmap,
    color: color,
    blocks: [16]u16
) : (i32, color) =
    loop (depth, nodecolor) = (0, color) while
        depth < 16
        && !is_opaque(nodecolor)
    do (
        depth + 1,
        match get_color (idmapping, blocks[15 - depth])
            case #none -> nodecolor
            case #color bg -> with_bg(nodecolor, bg) -- TODO: clamp opaque? otherwise opaque colors will get through even if there's non-opaque beneath
    )

-- returned heights are lower than inputs. Higher value means skywards
def rainin (
    idmapping: idmap,
    color: blockcolors,
    height: [16][16]i32,
    -- x y z, y is vertical
    blocks: [16][16][16]u16
) : [16][16](i32, color) =
    indices2d height
        |> map2d (\(z, x) ->
            let column = blocks[x,:,z]
            let dcol = raincol(
                idmapping,
                color[x,z],
                column
            )
            in (height[x,z] + dcol.0, dcol.1)
                -- [255, u8.i32(dcol.0), 0, 0]
        )

entry rain
    (id_ids: []u16)
    (id_colors: []color)
    (color: [16][16]color)
    (height: [16][16]i32)
    (blocks: [16*16*16]u16)
: ([16][16]i32, blockcolors) = 
    let idmapping : idmap = zip id_ids id_colors
    let ublocks : [16][16][16]u16 = unflatten_3d blocks
    in (rainin (idmapping, color, height, ublocks)) |> unzip2d