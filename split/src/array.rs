use super::{CHUNK_AREA_Y, Node};
use minetestworld::MAPBLOCK_LENGTH;

/// Stores (x,y) items, consecutive y consecutively
pub struct BlockSize2D<T>(pub [T; CHUNK_AREA_Y]);

impl<T> BlockSize2D<T> {
    pub fn iter_indices() -> impl Iterator<Item=Node<(u8, u8)>> {
        let xs = 0..MAPBLOCK_LENGTH;
        let ys = 0..MAPBLOCK_LENGTH;
        ys.flat_map(move |y| xs.clone().map(move |x| Node((x, y))))
    }
    fn flat_index(Node((x, y)): Node<(u8, u8)>) -> usize{
        x as usize * MAPBLOCK_LENGTH as usize + y as usize
    }
    pub fn get(&self, idx: Node<(u8, u8)>) -> Option<&T> {
        self.0.get(Self::flat_index(idx))
    }
}