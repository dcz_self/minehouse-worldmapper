pub mod array;

use minetestworld::{MAPBLOCK_LENGTH, map_block::MAPBLOCK_SHIFT};

/// The area of a block when viewed along the Y (vertical) axis
pub const CHUNK_AREA_Y: usize = MAPBLOCK_LENGTH as usize * MAPBLOCK_LENGTH as usize;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct Block<T>(pub T);
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Node<T>(pub T);

impl Block<(i16, i16)> {
    pub fn with_inner(&self, Node((a, b)): Node<(u8, u8)>) -> Node<(i32, i32)> {
        Node((
            self.0.0 as i32 * MAPBLOCK_LENGTH as i32 + a as i32,
            self.0.1 as i32 * MAPBLOCK_LENGTH as i32 + b as i32,
        ))
    }
}

impl Block<(i16, i16, i16)> {
    pub fn with_inner(&self, Node((a, b, c)): Node<(u8, u8, u8)>) -> Node<(i32, i32, i32)> {
        Node((
            self.0.0 as i32 * MAPBLOCK_LENGTH as i32 + a as i32,
            self.0.1 as i32 * MAPBLOCK_LENGTH as i32 + b as i32,
            self.0.2 as i32 * MAPBLOCK_LENGTH as i32 + c as i32,
        ))
    }
}

impl Node<(i32, i32, i32)> {
    pub fn to_block(&self) -> Block<(i16, i16, i16)> {
        #[inline]
        fn x_to_b(a: i32) -> i16 {
            // Not sure if the compiler can make a complex division fast
            (a >> MAPBLOCK_SHIFT) as i16
        }
        Block((x_to_b(self.0.0), x_to_b(self.0.1), x_to_b(self.0.2)))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn node_to_block() {
        assert_eq!(Node((1,1, 1)).to_block(), Block((0,0,0)));
        assert_eq!(Node((-1,-1, -1)).to_block(), Block((-1,-1,-1)));
    }
}